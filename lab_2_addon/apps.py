from django.apps import AppConfig


class Lab2AddonConfig(AppConfig):
    name = 'Lab_2_addon'
