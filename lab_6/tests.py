from .views import index
from django.test import TestCase
from django.test import Client
from django.urls import resolve
# Create your tests here.
class Lab6UnitTest(TestCase):
    def test_hello_name_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/lab-6/')
        self.assertEqual(found.func, index)
