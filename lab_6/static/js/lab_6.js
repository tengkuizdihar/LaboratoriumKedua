var themes = `[
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]`;

var selectedThemes = '{"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}';

localStorage.setItem("themes",themes);
localStorage.setItem("selectedThemes",selectedThemes);

$(document).ready(function() {
    $('.my-select').select2({'data': JSON.parse(localStorage.getItem("themes"))});
});

//Theme Change
$('.apply-button').on('click', function () {
    var changedTo = $('.my-select option:selected').text();
    var length = JSON.parse(localStorage.getItem("themes")).length;

    var bg = "";
    var font = "";

    for (var index = 0; index < length; index++) {
        if(JSON.parse(localStorage.getItem("themes"))[index]['text'] === changedTo){
            bg = JSON.parse(localStorage.getItem("themes"))[index]["bcgColor"];
            font = JSON.parse(localStorage.getItem("themes"))[index]["fontColor"];
            localStorage.setItem("selectedThemes", JSON.parse(localStorage.getItem("themes"))[index]);
        }
    }

    document.body.style.backgroundColor = bg;
    document.body.style.color = font;
});
//END

// Calculator
var message = document.getElementById('message-me');
var chatBox = document.getElementById('chat-box');
var print = document.getElementById('printing');
var erase = false;
var op = '';
var memory = 0;


var go = function (x) {
    if (x === 'ac') {
        print.value = 0;
        erase = true;
        memory = 0;
    
    } else if (x === 'eval') {
        if (!erase) {
            if (op === ' + ') {
                print.value = memory + parseFloat(print.value);
            } else if (op === ' - ') {
                print.value = memory - parseFloat(print.value);
            } else if (op === ' * ') {
                print.value = memory * parseFloat(print.value);
            } else if (op === ' / ') {
                print.value = memory / parseFloat(print.value);
            }
        }
        erase = true;

    } else if (x === ' + '){
        op = ' + ';
        memory = parseFloat(print.value);
        print.value = 0;
        erase = false;
    } else if (x === ' - ') {
        op = ' - ';
        memory = parseFloat(print.value);
        print.value = 0;
        erase = false;
    } else if (x === ' * ') {
        op = ' * ';
        memory = parseFloat(print.value);
        print.value = 0;
        erase = false;
    } else if (x === ' / ') {
        op = ' / ';
        memory = parseFloat(print.value);
        print.value = 0;
        erase = false;
    } else {
        if (print.value === "0") {
            if (x === ".") {
                print.value = "0";
            } else {
                print.value = "";   
            }
        }
        print.value += x;
    }
};

function sendMessage() {
    message.innerHTML += chatBox.value + '<br>';
    chatBox.value = "";
}

function evil(fn) {
    return new Function('return ' + fn)(); //This will return fn
}
// END

//Chat Message
$("textarea").keypress(function (event) {
    if (event.keyCode == 13 && !event.shiftKey) {
        sendMessage();
        return false;
    }
});
//END
