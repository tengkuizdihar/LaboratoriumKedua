python manage.py makemigrations
python manage.py migrate
python manage.py collectstatic --no-input
coverage run --include='lab_*/*' manage.py test
coverage report -m