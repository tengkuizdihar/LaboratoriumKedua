from django.shortcuts import render, redirect
from .models import Diary
from datetime import datetime
from django.contrib import messages
from pytz import timezone
import pytz as pytz
import json

'''
todayTime is for datetime-local minimum, to prevent 
any diary entries that're earlier than the time webpage 
is loading
'''
diary_dict = {}
def index(request):
    diary_dict = Diary.objects.all().values()
    response = {'diary_dict' : convert_queryset_into_json(diary_dict), 'todayTime' : datetime.today().isoformat(timespec='minutes')}
    return render(request, 'to_do_list.html', response)

'''
Add activity is now using WIB (+7 UTC)
'''
def add_activity(request):
    if request.method == 'POST':
        try:
            if len(request.POST['activity']) <= 2:
                messages.error(request, 'ERROR: Kegiatan tidak boleh di bahwa 2 karakter!')
                return redirect('/lab-3/')
            elif len(request.POST['activity']) > 60:
                messages.error(request, 'ERROR: Kegiatan melebihi 60 karakter!')
                return redirect('/lab-3/')
            date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
            WIB = pytz.timezone('Asia/Jakarta')
            Diary.objects.create(date=date,activity=request.POST['activity'])
            messages.success(request, 'SUCCESS: Kegiatan telah ditambah')
            return redirect('/lab-3/')
        except ValueError:
            messages.error(request, 'ERROR: Masukkan tidak didukung!')
            return redirect('/lab-3/')

def convert_queryset_into_json(queryset):
        ret_val = []
        for data in queryset:
            ret_val.append(data)
        return ret_val