from django.shortcuts import render
from lab_1.views import mhs_name, birth_date
#Create a content paragraph for your landing page:
landing_page_content = 'Welcome to my website. It is not the best in the world, but it is mine.'
#BUG the "\'" will not be recognized by test_landing_page_is_completed and WILL cause an error


def index(request):
    response = {'name': mhs_name, 'content': landing_page_content}
    return render(request, 'index_lab2.html', response)